# Minimal Spanning Tree #

Assignment in DA-ALG1000 - Algorithms course of spring 2014.

## Technology / topics ##
* [Minimal Spanning Tree](http://en.wikipedia.org/wiki/Minimum_spanning_tree)
* [Prim's Algorithm](http://en.wikipedia.org/wiki/Prim's_algorithm) 
* Big-O
* Program testing

## The program ##
The programs goal is to connect all nodes with minimal cost.
The nodes are "Houses" in this assignment, and the cost is money. So the task is to connect all houses for the smallest cost.

![MATRISE.PNG](https://bitbucket.org/repo/6oxzKd/images/1329811203-MATRISE.PNG)

Running the program the user can select which house is the starting point:

![2.PNG](https://bitbucket.org/repo/6oxzKd/images/2181139021-2.PNG)


Duble checking the matrix while making the program:

![test_av_matrise.png](https://bitbucket.org/repo/6oxzKd/images/2017290350-test_av_matrise.png)